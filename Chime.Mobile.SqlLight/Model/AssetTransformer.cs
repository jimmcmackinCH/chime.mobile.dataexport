﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chime.Application.Factory;
using Chime.Application.Model;
using Chime.Application.Model.CriteriaImplementations;
using Chime.Common;
using Chime.Application.ViewModel.Asset;
using System.Configuration;
using System.Data.SQLite;
using Newtonsoft.Json;
using LZStringCSharp;

namespace Chime.Mobile.SqlLight.Model
{
    public class AssetTransformer
    {
        private RepositoryFactory sourceFactory = null;
        private string sqlLiteConnectionString = @"Data Source=C:\Work\Source\CH\Chime.Mobile\Database\Chime.db; Version=3; FailIfMissing=True; Foreign Keys=True;";

        public AssetTransformer()
        {
            this.sourceFactory = new RepositoryFactory("Chime");
            this.sqlLiteConnectionString = ConfigurationManager.AppSettings["SqlLiteConnectionString"].ToString();
        }

        public void LoadAssets()
        {
            int pageSize = 1000;
            int pageCount = 0;
            var ac = new AssetCriteria();
            ac.PageSize = pageSize;
            ac.PageNumber = pageCount;
            ac.FullGraph = true;
            var repo = sourceFactory.AssetRepository;
            using (SQLiteConnection conn = new SQLiteConnection(this.sqlLiteConnectionString))
            {
                conn.Open();
                SQLiteCommand assetCmd = new SQLiteCommand(conn);
                SQLiteCommand assetSearchCmd = new SQLiteCommand(conn);
                {
                    int result = -1;
                    // combine two?
                    assetCmd.CommandText = "VACUUM; DELETE FROM Asset; DELETE FROM AssetSearch;";
                    result = assetCmd.ExecuteNonQuery();                   

                    assetCmd.CommandText = "INSERT INTO Asset(key, value) VALUES (@key, @value)";
                    assetCmd.Prepare();
                    assetSearchCmd.CommandText = " INSERT INTO AssetSearch(Id, AssetTypeId, AssetType, IsLocation, IsReferenceOnly, OpenQualityControlCaseIds, Name, Description, NameAndDescription,Location,ConstructionMonth,ConstructionYear,InventoryNo,Manufacturer,ManufacturerCountry,ManufacturerPartNumber,ManufacturerSerialNumber,ModelNumber,ObjectType,ObjectTypeDescription,Size,Status,UnitOfWeight,ValidFrom,ValidTo,Weight) " +
                        "VALUES (@Id, @AssetTypeId, @AssetType, @IsLocation, @IsReferenceOnly, @OpenQualityControlCaseIds, @Name, @Description, @NameAndDescription, @Location, @ConstructionMonth, @ConstructionYear, @InventoryNo, @Manufacturer, @ManufacturerCountry, @ManufacturerPartNumber, @ManufacturerSerialNumber, @ModelNumber, @ObjectType, @ObjectTypeDescription, @Size, @Status, @UnitOfWeight, @ValidFrom, @ValidTo, @Weight)";
                    assetSearchCmd.Prepare();
                    while (true)
                    {
                        ac.PageNumber = pageCount;
                        pageCount++;
                        var results = repo.Fetch(ac);
                        if (results.Count == 0)
                        {
                            break;
                        }
                        var transaction = conn.BeginTransaction();
                        foreach (var asset in results)
                        {
                            var vm = new AssetFullViewModel(asset);
                            var json = JsonConvert.SerializeObject(vm);
                            var zippedJson = LZString.CompressToUTF16(json);

                            var l = json.Length;
                            var lz = zippedJson.Length;

                            assetCmd.Parameters.AddWithValue("@key", asset.Id);
                            assetCmd.Parameters.AddWithValue("@value", zippedJson);

                            assetSearchCmd.Parameters.AddWithValue("@Id",asset.Id);
                            assetSearchCmd.Parameters.AddWithValue("@AssetTypeId", asset.AssetTypeId);
                            assetSearchCmd.Parameters.AddWithValue("@AssetType", asset.AssetType.Name);
                            assetSearchCmd.Parameters.AddWithValue("@IsLocation", asset.IsLocation);
                            assetSearchCmd.Parameters.AddWithValue("@IsReferenceOnly", asset.IsReferenceOnly);
                            assetSearchCmd.Parameters.AddWithValue("@OpenQualityControlCaseIds", 0);
                            assetSearchCmd.Parameters.AddWithValue("@Name", asset.Name);
                            assetSearchCmd.Parameters.AddWithValue("@Description", asset.Description);
                            assetSearchCmd.Parameters.AddWithValue("@NameAndDescription", vm.ND);
                            assetSearchCmd.Parameters.AddWithValue("@Location", asset.Location);
                            assetSearchCmd.Parameters.AddWithValue("@ConstructionMonth", asset.ConstructionMonth);
                            assetSearchCmd.Parameters.AddWithValue("@ConstructionYear", asset.ConstructionYear);
                            assetSearchCmd.Parameters.AddWithValue("@InventoryNo", asset.InventoryNo);
                            assetSearchCmd.Parameters.AddWithValue("@Manufacturer", asset.Manufacturer);
                            assetSearchCmd.Parameters.AddWithValue("@ManufacturerCountry", asset.ManufacturerCountry);
                            assetSearchCmd.Parameters.AddWithValue("@ManufacturerPartNumber", asset.ManufacturerPartNumber);
                            assetSearchCmd.Parameters.AddWithValue("@ManufacturerSerialNumber", asset.ManufacturerSerialNumber);
                            assetSearchCmd.Parameters.AddWithValue("@ModelNumber", asset.ModelNumber);
                            assetSearchCmd.Parameters.AddWithValue("@ObjectType", asset.ObjectType);
                            assetSearchCmd.Parameters.AddWithValue("@ObjectTypeDescription", asset.ObjectTypeDescription);
                            assetSearchCmd.Parameters.AddWithValue("@Size", asset.Size);
                            assetSearchCmd.Parameters.AddWithValue("@Status", asset.Status);
                            assetSearchCmd.Parameters.AddWithValue("@UnitOfWeight", asset.UnitOfWeight);
                            assetSearchCmd.Parameters.AddWithValue("@ValidFrom", asset.ValidFrom);
                            assetSearchCmd.Parameters.AddWithValue("@ValidTo", asset.ValidTo);
                            assetSearchCmd.Parameters.AddWithValue("@Weight", asset.Weight);

                            try
                            {
                                result = assetCmd.ExecuteNonQuery();
                                result = assetSearchCmd.ExecuteNonQuery();
                            }
                            catch (SQLiteException e)
                            {
                                var m = e.Message;
                            }
                        }
                        transaction.Commit();
                    }
                }
                conn.Close();

            }
        }
    }
}
