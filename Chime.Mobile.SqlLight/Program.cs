﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using Chime.Mobile.SqlLight.Model;

namespace Chime.Mobile.SqlLight
{
    class MobileAsset
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AssetTypeId { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var start = DateTime.Now;
            var at = new AssetTransformer();
            at.LoadAssets();
            var end = DateTime.Now;
        }

    }
}
